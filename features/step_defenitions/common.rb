Given /^I am on the login page$/ do
  start_time = Time.new.to_f
  @browser.open('https://miavchen.atlassian.net/login?dest-url=/wiki%2Findex.action&permission-violation=true')
  end_time = Time.new.to_f
  @homepage_load_time= (end_time - start_time)*1000.0
end


And /^I login using (.*) as username and (.*) as password$/ do |username, password|
  @username = username
  @password = password
  @browser.type(@browser.username, @username)
  @browser.type(@browser.password, @password)
  @browser.click(@browser.login_button)
end

Given /^I am logged in as an administrator$/ do
  steps %{
    Given I am on the login page
    And I login using mia.austria@gmail.com as username and atlassian123 as password
    Then I should be redirected to my main Dashboard
    }
end

Then(/^I should be redirected to my main Dashboard$/) do
  assert((@browser.current_url.include? '/wiki/dashboard.action'), "FAILURE: No Session")
end

And(/^I create a page$/) do
  steps %{
    And I click on Create button
    And I add a page title
    And I save my changes
    Then I should be able to see the newly created page
  }
end


And(/^I logout$/) do
  @browser.click(@browser.menu)
  @browser.click(@browser.logout_button)
end


And(/^I login as a normal user$/) do
  steps %{
    Given I am on the login page
    And I login using miavchen@icloud.com as username and atlassian123 as password
    Then I should be redirected to my main Dashboard
  }
end



