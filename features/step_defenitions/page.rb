And(/^I add a page title$/) do
  @new_page_title ="NewPage#{Time.new.to_i}"
  @browser.clear_text(@browser.content_title)
  @browser.type(@browser.content_title, @new_page_title)
end


And(/^I save my changes$/) do
  @browser.click(@browser.save_button)
end


Then(/^I should be able to see the newly created page$/) do
  assert((@browser.page_source.include? "#{@new_page_tite}"), "FAILURE: No Session")
end

And(/^I access the newly created page$/) do
  new_page_url = "https://miavchen.atlassian.net/wiki/display/TA/#{@new_page_title}"
  @browser.open("https://miavchen.atlassian.net/wiki/display/TA/#{@new_page_title}")
end

Then(/^I should be able to edit the page$/) do
  navigation_text =@browser.get_text(@browser.navigation)
  assert((navigation_text.include? "Edit"), "FAILURE: user can not edit page")
end


Then(/^I should not be able to edit the page$/) do
  navigation_text =@browser.get_text(@browser.navigation)
  assert(!(navigation_text.include? "Edit"), "FAILURE: user can still edit page")
end

And (/^I ensure that there are no restrictions$/) do
  @browser.click(@browser.space_tools)
  @browser.click(@browser.permissions)
  @browser.click(@browser.edit_permissions)
  if @browser.current_url.include? "authenticate"
    @browser.type(@browser.password, @password)
    @browser.click(@browser.confirm_button)
  end
  if @browser.check_select_status(@browser.normal_user_add_option) == false
    @browser.click(@browser.normal_user_select_all)
  end
  @browser.click(@browser.save_button)
end

And (/^I restrict a normal user to access the page$/) do
  @browser.click(@browser.space_tools)
  @browser.click(@browser.permissions)
  @browser.click(@browser.edit_permissions)
  if @browser.current_url.include? "authenticate"
    @browser.type(@browser.password, @password)
    @browser.click(@browser.confirm_button)
  end
  if@browser.check_select_status(@browser.normal_user_add_option)
    @browser.click(@browser.normal_user_add_option)
  end
  @browser.click(@browser.save_button)

end
