Feature: Login
  Login tests
  @ignore
  Scenario: As an administrator, I should be able to login
    Given I am on the login page
    And I login using mia.austria@gmail.com as username and atlassian123 as password
    Then I should be redirected to my main Dashboard

  @ignore
  Scenario: As a normal user, I should be able to login
    Given I am on the login page
    And I login using miavchen@icloud.com as username and atlassian123 as password
    Then I should be redirected to my main Dashboard





