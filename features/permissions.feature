Feature: Setting page permissions
  Homepage tests


  Scenario: As an administrator, I should be able to create a page
    Given I am logged in as an administrator
    And I click on Create button
    And I add a page title
    And I save my changes
    Then I should be able to see the newly created page

  Scenario: As a normal user, I should be able to edit a page with no restrictions
    Given I am logged in as an administrator
    And I create a page
    And I ensure that there are no restrictions
    And I logout
    And I login as a normal user
    And I access the newly created page
    Then I should be able to edit the page

  Scenario: As a normal user, I should not be able to edit a page with restrictions
    Given I am logged in as an administrator
    And I create a page
    And I restrict a normal user to access the page
    And I logout
    And I login as a normal user
    And I access the newly created page
    Then I should not be able to edit the page




