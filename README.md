# README #

## Tools ##
* Ruby – scripting language used
* Cucumber – to  write the test cases in a way that is readable to everyone
* Selenium Webdriver – used to interact with the browser
* Test::Unit – used in checking results

## Approach ##
Implemented page object model/ page handlers

## Prerequisites and Assumptions ##
The only assumption/pre requisite for these tests is that an admin account and a non admin account on miavchen.atlassian.net should have already been setup.