require 'pages/default'

module AutomationFramework
  class LoginPage < BasicPageHandler
    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/login/
    end

    def username
      {:id => 'username'}
    end

    def password
      {:id => 'password'}
    end

    def login_button
      {:id => 'login'}
    end

  end
end
