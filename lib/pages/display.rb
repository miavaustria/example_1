require 'pages/default'

module AutomationFramework
  class DisplayPage < BasicPageHandler

    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/wiki\/display/
    end

    def edit_link
      {:id => 'editPageLink'}
    end

    def logout_button
      {:id => 'logout-link'}
    end

    def menu
      {:id => 'user-menu-link'}
    end

    def space_tools
      {:id => 'space-tools-menu-trigger'}
    end

    def permissions
        {:css => "#inline-dialog-space-tools > div.aui-inline-dialog-contents.contents.aui-inline-dialog-no-shadow > div > div:nth-child(1) > ul > li:nth-child(2) > a"}
    end

    def navigation
      {:id => "navigation"}
    end
  end
end
