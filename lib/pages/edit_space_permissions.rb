require 'pages/default'

module AutomationFramework
  class EditSpacePermissionsPage < BasicPageHandler

    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/wiki\/spaces\/editspacepermissions/
    end

    def normal_user_view_option
      {:name => "confluence_checkbox_viewspace_user_ff8080814fb46205014fb46991980004"}
    end

    def save_button
      {:name => "save"}
    end

    def normal_user_add_option
      {:name => "confluence_checkbox_editspace_user_ff8080814fb46205014fb46991980004"}
    end

    def normal_user_select_all
      {:css => "#uPermissionsTable > tbody > tr:nth-child(4) > td:nth-child(1) > button"}
    end


  end
end
