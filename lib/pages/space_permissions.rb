require 'pages/default'

module AutomationFramework
  class SpacePermissionsPage < BasicPageHandler

    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/wiki\/spaces\/spacepermissions/
    end

    def edit_permissions
      {:id => 'edit'}
    end

    def save_button
      {:id => 'rte-button-publish'}
    end

    def logout_button
      {:id => 'logout-link'}
    end

    def menu
      {:id => 'user-menu-link'}
    end



  end
end
