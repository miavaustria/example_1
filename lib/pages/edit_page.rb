require 'pages/default'

module AutomationFramework
  class EditPage < BasicPageHandler

    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/wiki\/pages\/editpage/
    end

    def content_title
      {:id => 'content-title'}
    end

    def save_button
      {:id => 'rte-button-publish'}
    end


  end
end
