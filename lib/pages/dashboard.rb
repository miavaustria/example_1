require 'pages/default'

module AutomationFramework
  class DashboardPage < BasicPageHandler

    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/wiki\/dashboard/
    end

    def create_button
      {:id => 'quick-create-page-button'}
    end



  end
end
