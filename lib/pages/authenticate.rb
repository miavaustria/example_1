require 'pages/default'

module AutomationFramework
  class AuthenticatePage < BasicPageHandler

    def self.can_handle?(page)
      uri = URI.parse(page.current_url)
      uri.host =~ /miavchen\.atlassian\.net$/ and
          uri.path =~ /\/wiki\/authenticate/
    end

    def password
      {:id => 'password'}
    end

    def confirm_button
      {:id => 'authenticateButton'}
    end


  end
end
